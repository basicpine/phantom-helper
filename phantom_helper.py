import requests
import json
import sys
import os


token = os.environ.get("PHANTOM_API_KEY")
if token == None:
    print("Couldn't find enviroment variable called PHANTOM_API_KEY.")
    quit()

PHANTOM_SERVER =  "" #PUT YOUR PHANTOM SERVER URL HERE

def menu():
    print (r'''
      ,,                  ,                       
      ||      _          ||                       
 -_-_  ||/\\  < \, \\/\\ =||=  /'\\ \\/\\/\\                       ______
 || \\ || ||  /-|| || ||  ||  || || || || ||                    .-"      "-.
 || || || || (( || || ||  ||  || || || || ||                   /            \
 ||-'  \\ |/  \/\\ \\ \\  \\, \\,/  \\ \\ \\                  |              |
 |/      _/                                                   |,  .-.  .-.  ,|
                                                              | )(__/  \__)( |
  '                                                           |/     /\     \|
  ,,          ,,                                    (@_       (_     ^^     _)
  ||          ||                               _     ) \_______\__|IIIIII|__/__________________________
  ||/\\  _-_  || -_-_   _-_  ,._-_            (_)@8@8{}<________|-\IIIIII/-|___________________________>
  || || || \\ || || \\ || \\  ||                     )_/        \          /
  || || ||/   || || || ||/    ||                    (@           `--------` 
  \\ |/ \\,/  \\ ||-'  \\,/   \\,                   
   _/           |/                            by: MK     
         ''')
    print("""
USAGE:
    -r <filename.csv> Opens a phantom container and creates a artificat called RTR with a filename and cmd CEF feilds

    -u <filename.txt> Open a pahntom container and creates url artifacts from a .txt file. Flags urls with a custom CEF feild
                      called malicious for use in playbooks.

    -h Display this message
    """)
  
def error(r):
    if "failed to authenticate" in r["message"]:
        print("Request FAILED. Check your auth token.")

def add_malurl_artifact(container_id,PHANTOM_SERVER,filename):
    url = "{}/rest/artifact".format(PHANTOM_SERVER)
    headers = {"ph-auth-token": token}
    f = open(filename, 'r')
    malurls = f.readlines()

    for urls in malurls:
        artifact = {"name":urls,
                    "container_id":container_id,
                    "cef": {
                        "url":urls,
                        "malicious":"unknown"
                           },
                    "cef_types": {
                        "malicious": ["malicious"]
                            }
                    }

        post_data = json.dumps(artifact) 
        r = requests.post(url=url,data=post_data,headers=headers,verify=False) 

def add_rtr_artifact(container_id,PHANTOM_SERVER,filename,cmd,token):
    url = "{}/rest/artifact".format(PHANTOM_SERVER)
    headers = {"ph-auth-token": token}

    #data = {"file_name":"testname","cmd":"testcmd"}

    artifact = {"name":"RTR",
                "container_id":container_id,
                "cef": {
                        "filename":filename,
                        "cmd":cmd
                       },
                "cef_types": {
                        "cmd": ["cmd"]
                             }
                }

    post_data = json.dumps(artifact) 
    r = requests.post(url=url,data=post_data,headers=headers,verify=False)
    if r.json()['success'] == True:
        print("\nContainer opened sucessfully. See phantom for details.")

def rtr_container(label,name,PHANTOM_SERVER,filename,cmd,token):
    data = {"file_name":filename,"cmd":cmd}
    post_data = {"label": label,
             "name": name,
             "custom_fields": data
             }
    d = json.dumps(post_data)
    headers = {"ph-auth-token": token}
    url = "{}/rest/container".format(PHANTOM_SERVER)
    r = requests.post(url=url,data=d,headers=headers,verify=False)
    r_json = r.json()
    #Request Check
    try:
        if r_json["failed"] == True:
            error(r_json)
            container_id = False
    except:
        container_id = r.json()['id']
        return container_id


if len(sys.argv) < 3:
    menu()
else:
    if sys.argv[1] == "-r":
        filename = sys.argv[2]
        cmd = input("Please enter the cmd you wish to run: ")
        containername = input("Please enter a container name: ")
        id = rtr_container('ep_rtr',containername,PHANTOM_SERVER,filename,cmd,token)
        if id != False:
            add_rtr_artifact(id,PHANTOM_SERVER,filename,cmd,token)
        else:
            quit()
    elif sys.argv[1] == "-u":
        filename = sys.argv[2]
        cmd = "tmp"
        containername = input("Please enter a container name: ")
        id = rtr_container('ep_rtr',containername,PHANTOM_SERVER,filename,cmd,token)
        add_malurl_artifact(id,PHANTOM_SERVER,filename)

    elif sys.argv[1] == "-h":
        menu()
    else:
        print("invaled option")
        menu()
#add_artifact(container_id,PHANTOM_SERVER)